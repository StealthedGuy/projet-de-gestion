<?php
// Initialize the session
session_start();
 
// Check if the user is already logged in, if yes then redirect him to video page
if(isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true && $_SESSION["admin"] != 1){
    header("location: video.php");
    exit;
}

if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
?>


<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title>Admin Index</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/principal.css" />
    </head>
    <body>
        <!-- DEBUT: Entête fixe -->
        <header>
            <nav>
				 <ul>
				   <li><a href="admin_index.php">Admin Index</a></li>
				  <li><a href="admin_problem.php">Admin Problem</a></li>
				  <li><a href="admin_users.php">Admin Users</a></li>
				  <li><a href="logout.php">Logout</a></li>
				</ul> 
            </nav>
        </header>
        <!-- FIN: Entête fixe -->

        <!-- DEBUT: Partie principale de la page -->
        <main>
            <div id="contenu-principale">

            </div>
        </main>
        <!-- FIN: Partie principale de la page -->


    </body>
</html>