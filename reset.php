<?php
// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
 
// Include config file
require_once "Config.php";
 
// Define variables and initialize with empty values
$new_password = $confirm_password = "";
$new_password_err = $confirm_password_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
 
    // Validate new password
    if(empty(trim($_POST["new_password"]))){
        $new_password_err = "Please enter the new password.";     
    } elseif(strlen(trim($_POST["new_password"])) < 6){
        $new_password_err = "Password must have atleast 6 characters.";
    } else{
        $new_password = trim($_POST["new_password"]);
    }
    
    // Validate confirm password
    if(empty(trim($_POST["confirm_password"]))){
        $confirm_password_err = "Please confirm the password.";
    } else{
        $confirm_password = trim($_POST["confirm_password"]);
        if(empty($new_password_err) && ($new_password != $confirm_password)){
            $confirm_password_err = "Password did not match.";
        }
    }
        
    // Check input errors before updating the database
    if(empty($new_password_err) && empty($confirm_password_err)){
        // Prepare an update statement
        $sql = "UPDATE users SET password = ? WHERE id = ?";
        
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "si", $param_password, $param_id);
            
            // Set parameters
            $param_password = password_hash($new_password, PASSWORD_DEFAULT);
            $param_id = $_SESSION["id"];
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                // Password updated successfully. Destroy the session, and redirect to login page
                session_destroy();
                header("location: login.php");
                exit();
            } else{
                echo "Oops! Something went wrong. Please try again later.";
            }
        }
        
        // Close statement
        mysqli_stmt_close($stmt);
    }
    
    // Close connection
    mysqli_close($link);
}
?>
 
<!DOCTYPE html>
<html lang="en">
<html>
    <head>
        <title>Faction</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/principal.css" />
    </head>
    <body>
        <!-- DEBUT: Entête fixe -->
        <header>
            <div id="logo">
				<img src="images/logo.png" alt="Star Wars">
            </div>
            <nav>
				 <ul>
				   <li><a href="index.html">Accueil</a></li>
				  <li><a href="faction.html">Factions</a></li>
				  <li><a href="film.html">Films</a></li>
				  <li><a href="acteur.html">Acteurs</a></li>
				  <li><a href="register.php">Enregistrement</a></li>
				  <li><a href="login.php">Login</a></li>
				  
				
				</ul> 
				
				
            </nav>
				
        </header>
        <!-- FIN: Entête fixe -->

        <!-- DEBUT: Partie principale de la page -->
        <main>
		
            <div id="contenu-principale">
			
			
				<div class="flex-container"> 
                <div>
					<h2>Reset Password</h2>
					<p>Please fill out this form to reset your password.</p>
					<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post"> 
						<div class="form-group <?php echo (!empty($new_password_err)) ? 'has-error' : ''; ?>">
							<label>New Password</label>
							<input type="password" name="new_password" class="form-control" value="<?php echo $new_password; ?>">
							<span class="help-block"><?php echo $new_password_err; ?></span>
						</div>
						<div class="form-group <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
							<label>Confirm Password</label>
							<input type="password" name="confirm_password" class="form-control">
							<span class="help-block"><?php echo $confirm_password_err; ?></span>
						</div>
						<div class="form-group">
							<input type="submit" class="btn btn-primary" value="Submit">
							<a class="btn btn-link" href="video.php">Cancel</a>
						</div>
					</form>
				</div>
				</div>
			</div>
        </main>
        <!-- FIN: Partie principale de la page -->

        <!-- DEBUT: pied de page fixe -->
        <footer>
				<h1 class="a">  FAN DE STAR WARS. </h1>
			
				<p class="a"> -Courriel: fan_de_star_wars@hotmail.com - Tél. : (450)123-4567  -<a href="support.html">Support</a></li> </p>
        </footer>
        <!-- FIN: pied de page -->
    </body>
</html>