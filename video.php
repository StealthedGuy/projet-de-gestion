<?php
// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}
?>

<html>
    <head>
        <title>Video</title>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="css/principal.css" />
    </head>
    <body>
        <!-- DEBUT: Entête fixe -->
        <header>
            <div id="logo">
				<img src="images/logo.png" alt="Star Wars">
            </div>
             <nav>
				 <ul>
				   <li><a href="index.html">Accueil</a></li>
				  <li><a href="faction.html">Factions</a></li>
				  <li><a href="film.html">Films</a></li>
				  <li><a href="acteur.html">Acteurs</a></li>
				  <li><a href="logout.php">Logout</a></li>
				  <li><a href="reset.php">Reset Password</a></li>
				</ul> 
            </nav>
        </header>
        <!-- FIN: Entête fixe -->

        <!-- DEBUT: Partie principale de la page -->
        <main>
            <table> 
                <tr>
				<p>STAR WARS ÉPISODE I </p>
				<iframe width="560" height="315" src="https://www.youtube.com/embed/Fo--sWDK_nU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
					
				</tr>
				
				<tr>
					<p>STAR WARS ÉPISODE II </p>
					<iframe width="560" height="315" src="https://www.youtube.com/embed/gYbW1F_c9eM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</tr>
				
				<tr>
					<p>STAR WARS ÉPISODE III </p>
					<iframe width="560" height="315" src="https://www.youtube.com/embed/5UnjrG_N8hU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</tr>
				
				<tr>
				<p>STAR WARS ÉPISODE IV </p>
					<iframe width="560" height="315" src="https://www.youtube.com/embed/HKQgMXvgxsM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</tr>
				
				<tr>
					<p>STAR WARS ÉPISODE V </p>
					<<iframe width="560" height="315" src="https://www.youtube.com/embed/mdrcnZNXmTM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</tr>
				
				<tr>
					<p>STAR WARS ÉPISODE VI </p>
					<iframe width="560" height="315" src="https://www.youtube.com/embed/VUgIH6fyDvs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</tr>
				
				<tr>
					<p>STAR WARS ÉPISODE VII </p>
					<iframe width="560" height="315" src="https://www.youtube.com/embed/sGbxmsDFVnE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</tr>
				
				<tr>
					<p>STAR WARS ÉPISODE VIII </p>
					<iframe width="560" height="315" src="https://www.youtube.com/embed/Q0CbN8sfihY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</tr>
				
				<tr>
					<p>STAR WARS ÉPISODE IX </p>
					<iframe width="560" height="315" src="https://www.youtube.com/embed/i6pbI9niN4k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</tr>
				
				<tr>
					<p>ROGUE ONE A STAR WARS STORY </p>
					<iframe width="560" height="315" src="https://www.youtube.com/embed/frdj1zb9sMY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</tr>
				<tr>
					<p>SOLO A STAR WARS STORY </p>
					<iframe width="560" height="315" src="https://www.youtube.com/embed/jPEYpryMp2s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</tr>

            </table>
        </main>
        <!-- FIN: Partie principale de la page -->

        <!-- DEBUT: pied de page fixe -->
        <footer>
				<h1 class="a">  FAN DE STAR WARS. </h1>
			
				<p class="a"> -Courriel: fan_de_star_wars@hotmail.com - Tél. : (450)123-4567  -<a href="support.html">Support</a></li> </p>
        </footer>
        <!-- FIN: pied de page -->
    </body>
</html>